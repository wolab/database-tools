#!/bin/bash
# Usage: . <name>

S3_CONFIG=${S3_CONFIG:=$HOME/.s3cfg}

NAME=$1
JOB=$(configure get jobs "$NAME")

if [ "$JOB" == "null" ]; then
    echo "Job '$NAME' is not found."
    exit 1
fi

configure job "$NAME"

FROM=$(configure get jobs "$NAME" from)
TO=$(configure get jobs "$NAME" to)
TIME=$(date +%F-%H%M%S)

KEEP=$(configure get jobs "$NAME" keep)
KEEP=${KEEP:=30}

DATABASE=$(configure get credentials "$FROM" database)
BUCKET=$(configure get credentials "$TO" bucket)
FOLDER=$(configure get credentials "$TO" folder)
FILENAME=$DATABASE-$TIME.bak
DIRECTORY="s3://$BUCKET/$FOLDER/"

SQL_SERVER=$(configure get credentials "$FROM" server)
SQL_PORT=$(configure get credentials "$FROM" port)
SQL_USER=$(configure get credentials "$FROM" user)
SQL_PASSWORD=$(configure get credentials "$FROM" password)

S3_ENDPOINT=$(configure get credentials "$TO" endpoint)
S3_ACCESS_KEY=$(configure get credentials "$TO" id)
S3_SECRET_KEY=$(configure get credentials "$TO" secret)
S3_URI="s3://$S3_ENDPOINT/$BUCKET/$FOLDER/"

# exit if something fails
set -e

CREATE_CREDENTIAL="CREATE CREDENTIAL [s3://$S3_ENDPOINT] WITH IDENTITY = 'S3 Access Key', SECRET = '${S3_ACCESS_KEY}:${S3_SECRET_KEY}'"
DROP_CREDENTIAL="DROP CREDENTIAL [s3://$S3_ENDPOINT]"
BACKUP="BACKUP DATABASE [$DATABASE] TO URL = '$S3_URI$FILENAME' WITH FORMAT, COMPRESSION"

echo "Login to $DIRECTORY"
sqlcmd -S "$SQL_SERVER,$SQL_PORT" -U "$SQL_USER" -P "$SQL_PASSWORD" -C -Q "$CREATE_CREDENTIAL"
echo "Backing up $DATABASE to $DIRECTORY$FILENAME"
sqlcmd -S "$SQL_SERVER,$SQL_PORT" -U "$SQL_USER" -P "$SQL_PASSWORD" -C -Q "$BACKUP"
echo "Logout from $DIRECTORY"
sqlcmd -S "$SQL_SERVER,$SQL_PORT" -U "$SQL_USER" -P "$SQL_PASSWORD" -C -Q "$DROP_CREDENTIAL"

echo "Cleaning"
dumps=$(s3cmd --config "$S3_CONFIG" ls "$DIRECTORY$DATABASE" | awk '{print $NF}' | tac | tail -n+"$((KEEP + 1))")

for dump in $dumps; do
    s3cmd --config "$S3_CONFIG" del --recursive "$dump"
done
configure cleanup
