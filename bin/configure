#!/bin/bash
# Usage: . <name>

help() {
    echo "Read/Parse data from configuration file (path ~/configs.yaml)"
    echo
    echo "Usage: $(basename "$0") <command> <args>"
    echo
    echo "Command:"
    echo "  get <type> <name> <property>   : get credentials/jobs configuration"
    echo "  job <name>                     : prepare configurations for job"
    echo "  cleanup                        : cleanup configurations"
    exit 1
}

if [[ $# -lt 1 ]]; then
    help
fi

COMMAND=$1
shift 1
CONFIGURATION_FILE=${CONFIGURATION_FILE:-"configs.yaml"}

if [ ! -f "$CONFIGURATION_FILE" ]; then
    if [ ! -f "$HOME/configs.yaml" ]; then
        CONFIGURATION_FILE="/etc/configs.yaml"
    else
        CONFIGURATION_FILE="$HOME/configs.yaml"
    fi
fi

get() {
    TYPE=$1
    NAME=$2
    PROPERTY=$3
    shift 2

    FILTER="map(select(.name == \"$NAME\")) | .[0]"

    if [[ $# -eq 0 ]]; then
        yq ".$TYPE | $FILTER" "$CONFIGURATION_FILE"
    else
        yq ".$TYPE| $FILTER.$PROPERTY" "$CONFIGURATION_FILE"
    fi
}

get_all() {
    TYPE=$1

    yq ".$TYPE.[].name" "$CONFIGURATION_FILE"
}

generate() {
    NAME=$1
    TYPE=$(get credentials "$NAME" type)

    if [ "$TYPE" == "s3" ]; then
        config_file="$HOME/.s3cfg"

        echo "[default]" >"$config_file"
        {
            echo "host_base = $(get credentials "$NAME" endpoint)"
            echo "host_bucket = $(get credentials "$NAME" endpoint)"
            echo "access_key = $(get credentials "$NAME" id)"
            echo "secret_key = $(get credentials "$NAME" secret)"
        } >>"$config_file"
    elif [ "$TYPE" == "mongo" ]; then
        config_file="$HOME/.mongo.yaml"

        echo "uri: $(get credentials "$NAME" uri)" >"$config_file"
    fi
}

cleanup() {
    rm -rf "$HOME/.s3cfg"
    rm -rf "$HOME/.mongo.yaml"
}

job() {
    NAME=$1

    FROM=$(get jobs "$NAME" from)
    TO=$(get jobs "$NAME" to)

    generate "$FROM"
    generate "$TO"
}

$COMMAND "$@"
