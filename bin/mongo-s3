#!/bin/bash
# Usage: . <name>

S3_CONFIG=${S3_CONFIG:=$HOME/.s3cfg}
MONGO_CONFIG=${MONGO_CONFIG:=$HOME/.mongo.yaml}

NAME=$1
JOB=$(configure get jobs "$NAME")

if [ "$JOB" == "null" ]; then
  echo "Job '$NAME' is not found."
  exit 1
fi

configure job "$NAME"

FROM=$(configure get jobs "$NAME" from)
TO=$(configure get jobs "$NAME" to)
TIME=$(date +%F-%H%M%S)

KEEP=$(configure get jobs "$NAME" keep)
KEEP=${KEEP:=30}

DATABASE=$(configure get credentials "$FROM" database)
BUCKET=$(configure get credentials "$TO" bucket)
FOLDER=$(configure get credentials "$TO" folder)
FILENAME=$DATABASE-$TIME.gz
DIRECTORY="s3://$BUCKET/$FOLDER/"

# exit if something fails
set -e

mongodump --config "$MONGO_CONFIG" --db "$DATABASE" --gzip --archive="$FILENAME"
s3cmd --config "$S3_CONFIG" put "$FILENAME" "$DIRECTORY"
rm -rf "$FILENAME"

dumps=$(s3cmd --config "$S3_CONFIG" ls "$DIRECTORY$DATABASE" | awk '{print $NF}' | tac | tail -n+"$((KEEP + 1))")

for dump in $dumps; do
  s3cmd --config "$S3_CONFIG" del --recursive "$dump"
done

echo "Backup saved to $DIRECTORY$FILENAME"
configure cleanup
