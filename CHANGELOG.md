# [1.1.0](https://gitlab.com/wolab/database-tools/compare/v1.0.1...v1.1.0) (2024-06-20)


### Features

* backup and restore mssql ([bd1e38f](https://gitlab.com/wolab/database-tools/commit/bd1e38f415b62508b26eb0ecc1d5fef75c69be18))
* support declarative configuration ([d20db42](https://gitlab.com/wolab/database-tools/commit/d20db42af97770b31a280711dfe8210de0bfbd12))

## [1.0.1](https://gitlab.com/wolab/database-tools/compare/v1.0.0...v1.0.1) (2024-05-29)


### Bug Fixes

* add uid and gid ([101aa6a](https://gitlab.com/wolab/database-tools/commit/101aa6ad42dc8a49d01154cffb374c9b46d7840b))

# 1.0.0 (2024-05-29)


### Features

* backup and restore mongodb ([f38e7e2](https://gitlab.com/wolab/database-tools/commit/f38e7e29af1d0942b79e7f9b42474417b4a997d3))
