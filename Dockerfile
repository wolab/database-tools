FROM alpine:3.21.3 as root
ENV PATH=$PATH:/opt/mssql-tools18/bin
ENV GNUPGHOME $HOME/.gnupg
RUN mkdir -p $GNUPGHOME && chown -R $(whoami) $GNUPGHOME && chmod 700 $GNUPGHOME
RUN apk add mongodb-tools libc6-compat ca-certificates s3cmd gnupg bash curl yq --no-cache

# Install mssql tools following https://github.com/microsoft/mssql-docker/blob/master/linux/mssql-tools/Dockerfile.alpine
ARG MSODBCSQL_URL=https://download.microsoft.com/download/3/5/5/355d7943-a338-41a7-858d-53b259ea33f5/msodbcsql18_18.3.3.1-1_amd64
ARG MSSQLTOOL_URL=https://download.microsoft.com/download/3/5/5/355d7943-a338-41a7-858d-53b259ea33f5/mssql-tools18_18.3.1.1-1_amd64
RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --import - && \
    curl -s "$MSODBCSQL_URL.apk" -o msodbc.apk && \
    curl -s "$MSODBCSQL_URL.sig" -o msodbc.sig && \
    curl -s "$MSSQLTOOL_URL.apk" -o tools.apk && \
    curl -s "$MSSQLTOOL_URL.sig" -o tools.sig && \
    gpg --verify msodbc.sig msodbc.apk && \
    gpg --verify tools.sig tools.apk && \
    apk add --allow-untrusted msodbc.apk && \
    apk add --allow-untrusted tools.apk && \
    rm -f "*.apk" "*.sig"

COPY bin/ /bin/
COPY examples/configs-sample.yaml /etc/
RUN chmod +x /bin/*

FROM root
ARG USER=default
ENV HOME /home/$USER
ENV GNUPGHOME $HOME/.gnupg
WORKDIR $HOME
RUN adduser -u 1001 -s /bin/bash -D $USER
RUN mkdir -p /opt/data && chown -R $USER /opt/data
USER $USER
RUN mkdir -p $GNUPGHOME && chown -R $(whoami) $GNUPGHOME && chmod 700 $GNUPGHOME
